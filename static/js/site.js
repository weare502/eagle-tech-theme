/* jshint devel: true */
/* jshint ignore:start */
/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
		String.prototype.startsWith = function(searchString, position){
			position = position || 0;
			return this.substr(position, searchString.length) === searchString;
	};
}

/**
 * FitVids JS
 */
;(function( $ ){

	'use strict';

	$.fn.fitVids = function( options ) {
		var settings = {
			customSelector: null,
			ignore: null
		};

		if(!document.getElementById('fit-vids-style')) {
			// appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
			var head = document.head || document.getElementsByTagName('head')[0];
			var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
			var div = document.createElement("div");
			div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
			head.appendChild(div.childNodes[1]);
		}

		if ( options ) {
			$.extend( settings, options );
		}

		return this.each(function(){
			var selectors = [
				'iframe[src*="player.vimeo.com"]',
				'iframe[src*="youtube.com"]',
				'iframe[src*="youtube-nocookie.com"]',
				'iframe[src*="kickstarter.com"][src*="video.html"]',
				'object',
				'embed'
			];

			if (settings.customSelector) {
				selectors.push(settings.customSelector);
			}

			var ignoreList = '.fitvidsignore';

			if(settings.ignore) {
				ignoreList = ignoreList + ', ' + settings.ignore;
			}

			var $allVideos = $(this).find(selectors.join(','));
			$allVideos = $allVideos.not('object object'); // SwfObj conflict patch
			$allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

			$allVideos.each(function(){
				var $this = $(this);
				if($this.parents(ignoreList).length > 0) {
					return; // Disable FitVids on this video.
				}
				if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
				if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
				{
					$this.attr('height', 9);
					$this.attr('width', 16);
				}
				var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
						width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
						aspectRatio = height / width;
				if(!$this.attr('name')){
					var videoName = 'fitvid' + $.fn.fitVids._count;
					$this.attr('name', videoName);
					$.fn.fitVids._count++;
				}
				$this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
				$this.removeAttr('height').removeAttr('width');
			});
		});
	};
	
	// Internal counter for unique video names.
	$.fn.fitVids._count = 0;
	
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */


var freezeVp = function(e) {
    e.preventDefault();
};

window.stopBodyScrolling = function(bool) {
    if (bool === true) {
        document.body.addEventListener("touchmove", freezeVp, false);
    } else {
        document.body.removeEventListener("touchmove", freezeVp, false);
    }
};


jQuery( document ).ready( function( $ ) {
	var $body = $('body');
	$body.fitVids();

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});
	
	$('.magnific-trigger').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true
	});

	$('#menu-toggle').on('click', function(){
		$('#nav-main .nav-inner-wrap').toggleClass('visible-menu');
	});

	$('.home-slider').slick();

	$('.testimonials-slider').slick();

	$('.page-template-contact .contact-form-selection-button').on('click', function(){
		var target = $(this).attr('data-target');
		$('.contact-cta-cards .cta-card').removeClass('selected');
		$('.contact-form-option').removeClass('selected');

		$(this).closest('.cta-card').addClass('selected');
		$('#'+target).addClass('selected');
	});

	$('.landing-page-tab-nav .landing-page-tab').on('click', function(){
		var target = $(this).attr('data-target');
		$('.landing-page-tab-nav .landing-page-tab').removeClass('selected');
		$('.landing-page-tabs .landing-page-tab-content').removeClass('selected');

		$(this).addClass('selected');
		$('#'+target).addClass('selected');

		var element = jQuery('#'+target);
		var offset = element.offset().top;
		if(!element.is(":visible")) {
			element.css({"visibility":"hidden"}).show();
			offset = element.offset().top;
			element.css({"visibility":"", "display":""});
		}

		var visible_area_start = jQuery(window).scrollTop();
		var visible_area_end = visible_area_start + window.innerHeight;

		if(offset < visible_area_start || offset > visible_area_end){
			// Not in view so scroll to it
			jQuery('html,body').animate({scrollTop: offset - window.innerHeight/5}, 1000);
			return false;
		}
		return true;
	});


}); // End Document Ready