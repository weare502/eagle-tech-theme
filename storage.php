<?php
/**
 * Template Name: Storage
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

$ctas = get_field('footer_options', 'option');
$chosen_cta = intval(get_field('footer_call_to_action', $post->ID ));
$context['footer_cta'] = $ctas[$chosen_cta];

Timber::render( array( 'storage.twig', 'page.twig' ), $context );