<?php
/**
 * Template Name: Whitepaper Template
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

$ctas = get_field('footer_options', 'option');
$chosen_cta = intval(get_field('footer_call_to_action', $post->ID ));
if ($chosen_cta > -1){
    $context['footer_cta'] = $ctas[$chosen_cta];
}

Timber::render( array( 'whitepaper.twig', 'page.twig' ), $context );