<?php

$labels = array(
	'name'               => __( 'Case Studies', 'text-domain' ),
	'singular_name'      => __( 'Case Study', 'text-domain' ),
	'add_new'            => _x( 'Add New Case Study', 'text-domain', 'text-domain' ),
	'add_new_item'       => __( 'Add New Case Study', 'text-domain' ),
	'edit_item'          => __( 'Edit Case Study', 'text-domain' ),
	'new_item'           => __( 'New Case Study', 'text-domain' ),
	'view_item'          => __( 'View Case Study', 'text-domain' ),
	'search_items'       => __( 'Search Case Studies', 'text-domain' ),
	'not_found'          => __( 'No Case Studies found', 'text-domain' ),
	'not_found_in_trash' => __( 'No Case Studies found in Trash', 'text-domain' ),
	'parent_item_colon'  => __( 'Parent Case Study:', 'text-domain' ),
	'menu_name'          => __( 'Case Studies', 'text-domain' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-edit',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
		'editor',
		'thumbnail',
		'custom-fields',
	),
);

register_post_type( 'case_study', $args );