<?php

$labels = array(
	'name'               => __( 'Testimonials', 'text-domain' ),
	'singular_name'      => __( 'Testimonial', 'text-domain' ),
	'add_new'            => _x( 'Add New Testimonial', 'text-domain', 'text-domain' ),
	'add_new_item'       => __( 'Add New Testimonial', 'text-domain' ),
	'edit_item'          => __( 'Edit Testimonial', 'text-domain' ),
	'new_item'           => __( 'New Testimonial', 'text-domain' ),
	'view_item'          => __( 'View Testimonial', 'text-domain' ),
	'search_items'       => __( 'Search Testimonials', 'text-domain' ),
	'not_found'          => __( 'No Testimonials found', 'text-domain' ),
	'not_found_in_trash' => __( 'No Testimonials found in Trash', 'text-domain' ),
	'parent_item_colon'  => __( 'Parent Testimonial:', 'text-domain' ),
	'menu_name'          => __( 'Testimonials', 'text-domain' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-testimonial',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
		'thumbnail',
		'custom-fields',
	),
);

register_post_type( 'testimonial', $args );
