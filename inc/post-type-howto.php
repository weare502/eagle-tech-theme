<?php

$labels = array(
	'name'               => __( 'How-To\'s', 'text-domain' ),
	'singular_name'      => __( 'How-To', 'text-domain' ),
	'add_new'            => _x( 'Add New How-To', 'text-domain', 'text-domain' ),
	'add_new_item'       => __( 'Add New How-To', 'text-domain' ),
	'edit_item'          => __( 'Edit How-To', 'text-domain' ),
	'new_item'           => __( 'New How-To', 'text-domain' ),
	'view_item'          => __( 'View How-To', 'text-domain' ),
	'search_items'       => __( 'Search How-To\'s', 'text-domain' ),
	'not_found'          => __( 'No How-To\'s found', 'text-domain' ),
	'not_found_in_trash' => __( 'No How-To\'s found in Trash', 'text-domain' ),
	'parent_item_colon'  => __( 'Parent How-To:', 'text-domain' ),
	'menu_name'          => __( 'How-To\'s', 'text-domain' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-editor-help',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
		'editor',
		'thumbnail',
		'custom-fields',
	),
);

register_post_type( 'howto', $args );

$labels = array(
	'name'                  => _x( 'How To Category', 'Taxonomy Category', 'text-domain' ),
	'singular_name'         => _x( 'Category', 'Taxonomy Category', 'text-domain' ),
	'search_items'          => __( 'Search Categories', 'text-domain' ),
	'popular_items'         => __( 'Popular Categories', 'text-domain' ),
	'all_items'             => __( 'All Categories', 'text-domain' ),
	'parent_item'           => __( 'Parent Category', 'text-domain' ),
	'parent_item_colon'     => __( 'Parent Category', 'text-domain' ),
	'edit_item'             => __( 'Edit Category', 'text-domain' ),
	'update_item'           => __( 'Update Category', 'text-domain' ),
	'add_new_item'          => __( 'Add New Category', 'text-domain' ),
	'new_item_name'         => __( 'New Category Name', 'text-domain' ),
	'add_or_remove_items'   => __( 'Add or remove Categories', 'text-domain' ),
	'choose_from_most_used' => __( 'Choose from most used Categories', 'text-domain' ),
	'menu_name'             => __( 'How To Categories', 'text-domain' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => false,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => false,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'howto_category', array( 'howto' ), $args );