<?php

$labels = array(
	'name'               => __( 'Events', 'text-domain' ),
	'singular_name'      => __( 'Event', 'text-domain' ),
	'add_new'            => _x( 'Add New Event', 'text-domain', 'text-domain' ),
	'add_new_item'       => __( 'Add New Event', 'text-domain' ),
	'edit_item'          => __( 'Edit Event', 'text-domain' ),
	'new_item'           => __( 'New Event', 'text-domain' ),
	'view_item'          => __( 'View Event', 'text-domain' ),
	'search_items'       => __( 'Search Events', 'text-domain' ),
	'not_found'          => __( 'No Events found', 'text-domain' ),
	'not_found_in_trash' => __( 'No Events found in Trash', 'text-domain' ),
	'parent_item_colon'  => __( 'Parent Event:', 'text-domain' ),
	'menu_name'          => __( 'Events', 'text-domain' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-calendar-alt',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
		'editor',
		'thumbnail',
		'custom-fields',
	),
);

register_post_type( 'event', $args );


$labels = array(
	'name'                  => _x( 'City/State', 'Taxonomy City/State', 'text-domain' ),
	'singular_name'         => _x( 'City/State', 'Taxonomy City/State', 'text-domain' ),
	'search_items'          => __( 'Search City/State', 'text-domain' ),
	'popular_items'         => __( 'Popular City/State', 'text-domain' ),
	'all_items'             => __( 'All City/State', 'text-domain' ),
	'parent_item'           => __( 'Parent City/State', 'text-domain' ),
	'parent_item_colon'     => __( 'Parent City/State', 'text-domain' ),
	'edit_item'             => __( 'Edit City/State', 'text-domain' ),
	'update_item'           => __( 'Update City/State', 'text-domain' ),
	'add_new_item'          => __( 'Add New City/State', 'text-domain' ),
	'new_item_name'         => __( 'New City/State Name', 'text-domain' ),
	'add_or_remove_items'   => __( 'Add or remove City/State', 'text-domain' ),
	'choose_from_most_used' => __( 'Choose from most used City/State', 'text-domain' ),
	'menu_name'             => __( 'City/State', 'text-domain' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => false,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => false,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'event_location', array( 'event' ), $args );


$labels = array(
	'name'                  => _x( 'Category', 'Taxonomy Category', 'text-domain' ),
	'singular_name'         => _x( 'Category', 'Taxonomy Category', 'text-domain' ),
	'search_items'          => __( 'Search Categories', 'text-domain' ),
	'popular_items'         => __( 'Popular Categories', 'text-domain' ),
	'all_items'             => __( 'All Categories', 'text-domain' ),
	'parent_item'           => __( 'Parent Category', 'text-domain' ),
	'parent_item_colon'     => __( 'Parent Category', 'text-domain' ),
	'edit_item'             => __( 'Edit Category', 'text-domain' ),
	'update_item'           => __( 'Update Category', 'text-domain' ),
	'add_new_item'          => __( 'Add New Category', 'text-domain' ),
	'new_item_name'         => __( 'New Category Name', 'text-domain' ),
	'add_or_remove_items'   => __( 'Add or remove Categories', 'text-domain' ),
	'choose_from_most_used' => __( 'Choose from most used Categories', 'text-domain' ),
	'menu_name'             => __( 'Event Categories', 'text-domain' ),
);

$args = array(
	'labels'            => $labels,
	'public'            => true,
	'show_in_nav_menus' => false,
	'show_admin_column' => true,
	'hierarchical'      => true,
	'show_tagcloud'     => false,
	'show_ui'           => true,
	'query_var'         => true,
	'rewrite'           => true,
	'query_var'         => true,
	'capabilities'      => array(),
);

register_taxonomy( 'event_category', array( 'event' ), $args );