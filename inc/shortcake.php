<?php

add_shortcode('recipe-embed', function($attr){
    $attr = wp_parse_args( $attr, array(
        'recipe' => false,
        'subtitle' => ''
    ) );

    if ( ! $attr['recipe'] ){
        return "<p style='background: red; color: white; padding: .5rem;'>No Recipe ID present in Shortcode</p>";
    }

    $context = Timber::get_context();
    $context['post'] = Timber::get_post( absint( $attr['recipe'] ) );
    $context['subtitle'] = esc_html( $attr['subtitle'] );
    $ret = Timber::compile( 'recipe-embed.twig', $context );
    
    return $ret;
} );

if ( ! function_exists( 'shortcode_ui_register_for_shortcode' ) ) {
    return; // bail early if shortcake isn't installed
}

shortcode_ui_register_for_shortcode(
    'recipe-embed', array(
        'label' => 'Recipe Embed',
        'listItemImage' => 'dashicons-carrot',
        'attrs' => array(
            array(
                'label' => 'Recipe to Embed',
                'attr' => 'recipe',
                'type' => 'post_select',
                'query' => array( 'post_type' => 'recipe' )
            ),
            array(
                'label' => 'Subtitle',
                'description' => 'A subtitle to go along with the title of the embed (name of recipe)',
                'attr' => 'subtitle',
                'type' => 'text'
            )
        )
    )
);
