# 502 Starter Theme
Yeah, I know, every company has a starter theme. What makes this one special? Well, not much. 

¯\\\_(ツ)\_/¯

## Installation
Before you can "install", you'll need a few tools.

- `CodeKit 3`
- `Timber for WordPress` plugin
- `Better FontAwesome` plugin
- `Maybe something else eventually`

Codekit is used to compile CSS, minify JS, reload the browser, and optimize images. There's also some slight configurations for browser reloading when you edit a `.twig` file. Plugins are used for plugin things.

## Usage
After you get Codekit configured and running, you can use it's built in browser refresh tech to develop your site if that's how you roll.

Some quick notes about the organiztation of the project:

### CSS
- ALL variables go in the `_variables.scss` file. If you have a bazillion variables, ask me if you can split them up.
- There should be one SCSS file for each custom page template or custom post type and they should be named appropriately. This makes finding CSS rules really really easy. CSS class names in this file should be named appropriately for the file and component they reference.
- If you reuse a pattern in more than one place, then you can consider adding it to the `_global-patterns.scss` file. These class names should be fairly generic.
- Media Queries should be placed inline on the element they affect if at all possible. *DO NOT place all the media queries at the bottom of the file.* Yo' momma raised you better than that!

### JS
- Most JS can go in the `site.js` file. jQuery and Underscore are included by default.
- If you are developing a really in depth feature for a JS driven experience, create a new JS file and enqueue it normally. So basically if the interaction takes more than 10-20 lines of JS, you probably need a new file.
- Vue.js apps should have their own file.

### PHP
- Custom Page Templates should generally have their own `.twig` files. It makes adding new code easy to reason about.
- Single CPT templates should also have their own `.twig` files. So if you have a CPT for `Actors`, you would also have a `single-actor.twig` file.
- When possible try to use the WordPress template heirachy before resorting to routing your templates via PHP.

### Extra Libraries & Frameworks
- Install dependencies via Bower in CodeKit if possible. If Bower ever really falls off the face of the earth, ignore me.