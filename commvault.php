<?php
/**
 * Template Name: Commvault
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

// function used as adding to $context[] will render it above the page
function call_gform() {
	gravity_form(22, false, false, false);
}

$ctas = get_field('footer_options', 'option');
$chosen_cta = intval(get_field('footer_call_to_action', $post->ID ));
$context['footer_cta'] = $ctas[$chosen_cta];

$templates = array( 'commvault.twig' );

Timber::render( $templates, $context );