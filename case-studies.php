<?php
/**
 * Template Name: Case Studies Archive
 */

$context = Timber::get_context();
$archive_id = get_option('page_for_case_study');
$context['posts'] = Timber::get_posts();

$post = new TimberPost($archive_id);
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

$ctas = get_field('footer_options', 'option');
$chosen_cta = intval( get_field('footer_call_to_action', $post->ID ) );
$context['footer_cta'] = $ctas[$chosen_cta];

$templates = array( 'case-studies.twig' );

Timber::render( $templates, $context );
