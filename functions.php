<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class EagleTechSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );

		add_action( 'init', array( $this, 'register_shortcake') );
		add_action( 'init', array( $this, 'add_acf_options_page' ) );

		add_action( 'pre_get_posts', array( $this, 'sort_events' ) );
		add_action( 'pre_get_posts', array( $this, 'all_downloads' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'dashboard_glance_items', array( $this, 'dashboard_glance_items' ) );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

		add_filter( 'acf/load_field/name=footer_call_to_action', array( $this, 'footer_cta_items' ) );

		add_action( 'init', function(){
			add_editor_style('style.css' );
		} );

		add_action( 'gform_after_submission_4', function( $entry, $form ){
			// Set a third parameter to specify a cookie expiration time, 
			// otherwise it will last until the end of the current session.
			setcookie( 'eagletech_downloads_form_submitted', 'true', time()+60*60*24*30, '/' );
		}, 10, 2 );

		// add_action( 'template_redirect', function(){
		// 	if( is_post_type_archive( 'download' ) && ! isset( $_COOKIE['eagletech_downloads_form_submitted'] ) && $_COOKIE['eagletech_downloads_form_submitted']	 ) {
		// 		wp_redirect( home_url( "/access-downloads" ) );
		// 		exit();
		// 	}
		// } );

		add_action( 'init', function(){
			if ( isset( $_GET['download'] ) ){
				// if ( isset( $_COOKIE['eagletech_downloads_form_submitted'] ) && $_COOKIE['eagletech_downloads_form_submitted'] ){
					$dl = Timber::get_post(intval($_GET['download']));
					
					if ( ! $dl ) return;

					if ($dl->get_field('external_file')){
						wp_redirect( $dl->get_field('external_file') );
						exit();
					}

					if ( $dl->get_field('file') ){
						wp_redirect( $dl->get_field('file')['url'] );
						exit();
					}

					return;
				// } else {
				// 	wp_redirect( home_url('/access-downloads/') );
				// 	exit();
				// // }
			}
		} );

		// add_action( 'template_redirect', function(){
		// 	if( is_page('access-downloads') && isset( $_COOKIE['eagletech_downloads_form_submitted'] ) && $_COOKIE['eagletech_downloads_form_submitted'] ) {
		// 		wp_redirect( home_url( "/downloads/" ) );
		// 		exit();
		// 	}
		// } );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		// $context['plugin_content'] = TimberHelper::ob_function( 'the_content' );
		$context['testimonials'] = Timber::get_posts(array( 'post_type' => 'testimonial', 'posts_per_page' => 100, 'orderby' => 'rand' ));
		$context['all_events_link'] = get_permalink( get_option('page_for_event') );

		$context['upcoming_events'] = Timber::get_posts( array(
			'post_type'  => 'event',
			'posts_per_page' => 4,
			'meta_query' => array(
				array(
					'key' => 'start_date',
					'compare' => '>=',
					'value' => date('Ymd'),
				)
			),
			'orderby'     	 => 'meta_value_num',
			'order'          => 'ASC',
		) );
		
		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Navigation' );
		// Images Sizes
		add_image_size( 'xlarge', 2880, 2000 );
	}

	function enqueue_scripts(){
		// Dependencies
		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120206' );
		wp_enqueue_style( 'mcc-slick-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css', '20120206' );
		wp_enqueue_style( 'mcc-slick-theme-css', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css', '20120206' );
		
		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'mcc-slick', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'mcc-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup' ), '20160823' );
		// wp_enqueue_script( 'mcc-video', get_template_directory_uri() . "/static/js/container.player.min.js", array( 'jquery', 'underscore' ), '20160820', true );
		// wp_enqueue_script( 'mcc-vue', get_template_directory_uri() . "/static/js/vue.js", array( 'jquery', 'underscore' ), '20160821', true );
		
		wp_localize_script( 'magnific-popup', "wpThemeUrl", get_stylesheet_directory_uri() );
	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
			.wrap .error { display: none !important; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
			// array(  
			// 	'title'    => 'Gold Text',  
			// 	// 'selector' => '*',
			// 	'inline' => 'span',
			// 	'classes'  => 'gold-text',
			// 	// font awesome must be available in the admin area to see the icon
			// 	'icon'     => ' fa fa-eye-dropper'
			// ),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;  
	  
	}

	function register_post_types(){
		include 'inc/post-type-testimonial.php';
		include 'inc/post-type-event.php';
		include 'inc/post-type-download.php';
		include 'inc/post-type-howto.php';
		include 'inc/post-type-case-study.php';
	}

	function register_shortcake(){
		include 'inc/shortcake.php';
	}

	function dashboard_glance_items( $items ){
		foreach ( get_post_types(array('public'=>true)) as $post_type ){
			$num_posts = wp_count_posts( $post_type );
			if ( $num_posts && $num_posts->publish ) {
				if ( 'post' == $post_type ) {
					continue;
				}
				if ( 'page' == $post_type ) {
					continue;
				}
				$post_type_object = get_post_type_object( $post_type );
				$text = _n( '%s ' . $post_type_object->labels->singular_name, '%s ' . $post_type_object->label, $num_posts->publish );
				$text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
				if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) ) {
					$items[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
				} else {
					$items[] = sprintf( '<span>%2$s</span>', $post_type, $text );
				}
			}
		}
		
		return $items;
	}

	function sort_events( $query ){
		if ( is_admin() ) return;
		if( ! $query->is_main_query() ) return;
	    if( ! is_post_type_archive( 'event' ) ) return;

	    $query->set('meta_query', array(
				array(
					'key' => 'start_date',
					'compare' => '>=',
					'value' => date('Ymd'),
				)
			)
		);
	    $query->set('orderby', 'meta_value_num');
	    $query->set('order', 'ASC');
	}

	function all_downloads( $query ){
		if ( is_admin() ) return;
		if( ! $query->is_main_query() ) return;
	    if( ! is_post_type_archive( 'download' ) ) return;
	    $query->set('posts_per_page', 1000);
	    $query->set('orderby', 'post_title');
	    $query->set('order', 'ASC');
	}

	function add_acf_options_page(){
		if ( ! function_exists('acf_add_options_page') ){
			return;
		}

		acf_add_options_page(array(
			'page_title' 	=> 'Site Options',
			'menu_title'	=> 'Site Options',
			'menu_slug' 	=> 'eagle-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));

		acf_add_options_page(array(
			'page_title' 	=> 'Partners',
			'menu_title'	=> 'Partners',
			'menu_slug' 	=> 'eagle-partners',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	function footer_cta_items( $field ){
		$ctas = get_field('footer_options', 'option');
		foreach ( $ctas as $key => $cta ){
			$field['choices'][$key] = $cta['color'] . ' - ' . $cta['title'];
		}
		$field['choices'][-1] = 'none';
		return $field;
	}

}

new EagleTechSite();

function eatwheat_render_primary_menu(){ // used in base.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
		'menu_class' => '',
		'menu_id' => 'primary-menu',
	) );
}