<?php
/**
 * Template Name: iFrame Content
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

Timber::render( array( 'iframe.twig', 'page.twig' ), $context );