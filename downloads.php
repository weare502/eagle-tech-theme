<?php
/**
 * Template Name: Downloads Archive
 */

$context = Timber::get_context();
$archive_id = get_option('page_for_download');
$context['posts'] = Timber::get_posts();

$post = new TimberPost($archive_id);
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;

$ctas = get_field('footer_options', 'option');
$chosen_cta = intval(get_field('footer_call_to_action', $post->ID ));
$context['footer_cta'] = $ctas[$chosen_cta];

$templates = array( 'downloads.twig' );

Timber::render( $templates, $context );
